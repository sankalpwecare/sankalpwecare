from django.db import models

# Create your models here.

class donar(models.Model):
    full_name = models.CharField(max_length=100)
    amount = models.IntegerField()
    campign_name = models.CharField(max_length=100)
    donate_time = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.full_name


class causes(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()
    raised_from = models.IntegerField()
    raised_to = models.IntegerField()
    last_donation = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title


class volunterForm(models.Model):
    name = models.CharField(max_length=50)
    mobile_number = models.IntegerField(max_length=10)
    email = models.EmailField(blank=False, null=False)
    message = models.TextField(blank=True, null=True)
    def __str__(self):
        return self.name


