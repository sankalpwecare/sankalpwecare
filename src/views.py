from django.shortcuts import render
from .models import donar, causes, volunterForm
from .forms import volunterForms
from django.shortcuts import redirect

# Create your views here.


def index(request):
    get_donar = donar.objects.all();
    get_causes = causes.objects.all();
    registrationComplete = False
    form = volunterForms(request.POST)
    if request.method == 'POST':
        print('i am here')
        if form.is_valid():
            name = form.cleaned_data['name']
            mobile_number = form.cleaned_data['mobile_number']
            email = form.cleaned_data['email']
            message = form.cleaned_data['message']
            formInstance = volunterForm(name=name, mobile_number=mobile_number, email=email, message=message)
            formInstance.save()
            registrationComplete = True
            print(name)
            return redirect('/thankyou')
            
    context = {
        'donar': get_donar,
        'causes': get_causes,
        'form': form,
        'flag': registrationComplete
    }
    return render(request, 'index.html', context)

def thank_you(request):
    return render(request, 'thank_you.html')