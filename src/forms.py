from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit
class volunterForms(forms.Form):
    name = forms.CharField(required=True)
    mobile_number = forms.IntegerField(required=True)
    email = forms.EmailField(required=True)
    message = forms.CharField(required=False, widget=forms.Textarea)
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper 
        self.helper.form_method = 'post'
        self.helper.layout = Layout(
             'name',
             'mobile_number',
             'email',
             'message',
             Submit('submit', 'Become Volunteer', css_class="btn-success")
        )
   
